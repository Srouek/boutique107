﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="AjouterArticle.aspx.cs" Inherits="AFCEPF.AI107.Boutique.Front.AjouterArticle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/NewArticle.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">
    <div id="NewArticle">
    <label>Nom de l'article</label>
    <asp:TextBox id="txtNom" runat="server"></asp:TextBox>
        <asp:Label id="lblErreur" runat="server" Text="Label"></asp:Label>

    <label>Description: </label>
    <asp:TextBox id="txtDescription" runat="server"></asp:TextBox>

    <label>Prix : </label>
    <asp:TextBox id="txtPrix" runat="server"></asp:TextBox>

    <label>Stock : </label>
    <asp:TextBox id="txtStock" runat="server"></asp:TextBox>

    <label>Son Rayon</label>
    <asp:DropDownList id="ddlRayons" runat="server"></asp:DropDownList>

    <asp:Button id="btnAjouter" runat="server" Text="Button" OnClick="btnAjouter_Click" />
        </div>

</asp:Content>
