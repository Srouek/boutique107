﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="Accueil.aspx.cs" Inherits="AFCEPF.AI107.Boutique.Front.Accueil" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">
    
    <h2>Bienvenue dans ma boutique !</h2>
    
    <asp:DropDownList ID="ddlRayons" runat="server"></asp:DropDownList>
    <asp:Button ID="btnTestRayons" runat="server" Text="OK" OnClick="btnTestRayons_Click" />
    <asp:Label ID="lblIdRayon" runat="server" Text=""></asp:Label>

    <asp:BulletedList ID="bulArticles" runat="server" DisplayMode="HyperLink"></asp:BulletedList>

</asp:Content>
