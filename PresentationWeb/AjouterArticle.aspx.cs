﻿using AFCEPF.AI107.Boutique.Business;
using AFCEPF.AI107.Boutique.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AFCEPF.AI107.Boutique.Front
{
    public partial class AjouterArticle : System.Web.UI.Page
    {
        CatalogueBU bu = new CatalogueBU();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                List<RayonEntity> rayons = bu.GetListeRayons();

                rayons.Add( new RayonEntity(-1, "Aucun"));

                // J'insère mes valeur dans la drop down list
                ddlRayons.DataTextField = "Libelle";
                ddlRayons.DataValueField = "Id";
                ddlRayons.DataSource = rayons;
                ddlRayons.DataBind();
            }

        }

        protected void btnAjouter_Click(object sender, EventArgs e)
        {
            // Je déclare mon objet à alimenter
            ArticleEntity article = new ArticleEntity();

            // Je récupère les valeurs venu de la requete
            try
            {
                article.Nom = txtNom.Text;
                article.Description = txtDescription.Text;
                article.PrixUnitaire = float.Parse(txtPrix.Text);
                article.Stock = int.Parse(txtStock.Text);
                article.IdRayon = int.Parse(ddlRayons.SelectedValue);

                bu.AjouterArticle(article);
            }
            catch(Exception exc)
            {
                lblErreur.Text = exc.Message;
            }
               

                



            
        }


    }
}