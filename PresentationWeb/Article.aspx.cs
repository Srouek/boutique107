﻿using AFCEPF.AI107.Boutique.Business;
using AFCEPF.AI107.Boutique.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AFCEPF.AI107.Boutique.Front
{
    public partial class Article : System.Web.UI.Page
    {
        CatalogueBU bu = new CatalogueBU();
        protected void Page_Load(object sender, EventArgs e)
        {
            int idArticle = 0;
            int.TryParse(Request["id"], out idArticle);
            ArticleEntity article = bu.GetArticle(idArticle); 
            // DATABINDING :

            // nom du champ à utiliser pour l'affichage :

            // Si j'ai récupéré l'article je l'affiche :
            if(article != null)
            {

                lblArticle.Text = article.Nom;
                lblDescription.Text = article.Description;
                imgPhoto.ImageUrl = "images/" + article.Photo;
                imgPhoto.AlternateText = article.Nom;
                lblPrix.Text = article.PrixUnitaire.ToString();
                lblStock.Text = article.Stock.ToString();

            }

           
            
            



        }
    }
}