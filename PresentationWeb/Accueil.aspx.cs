﻿using AFCEPF.AI107.Boutique.Business;
using AFCEPF.AI107.Boutique.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AFCEPF.AI107.Boutique.Front
{
    public partial class Accueil : System.Web.UI.Page
    {
        CatalogueBU bu = new CatalogueBU();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<RayonEntity> rayons = bu.GetListeRayons();

                rayons.Insert(0, new RayonEntity(-1, "Tous les rayons"));

                // DATABINDING :

                // nom du champ à utiliser pour l'affichage :
                ddlRayons.DataTextField = "Libelle";
                // nom du champ à utiliser pour la valeur choisie : 
                ddlRayons.DataValueField = "Id";
                ddlRayons.DataSource = rayons;
                ddlRayons.DataBind();

                bulArticles.DataTextField = "Nom";
                //bulArticles.DataTextFormatString = "{0}";
                bulArticles.DataSource = bu.GetListeArticles();
                bulArticles.DataBind();
            }
        }

        protected void btnTestRayons_Click(object sender, EventArgs e)
        {
            List<ArticleEntity> articles = new List<ArticleEntity>();
            // récupération de la valeur sélectionnée :
            lblIdRayon.Text = ddlRayons.SelectedValue;
           // int idRayon = int.Parse(ddlRayons.SelectedValue);
           

            
            bulArticles.DataSource = bu.GetListeArticles(int.Parse(ddlRayons.SelectedValue));
            bulArticles.DataBind();
        }
    }
}