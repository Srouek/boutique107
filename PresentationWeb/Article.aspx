﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="Article.aspx.cs" Inherits="AFCEPF.AI107.Boutique.Front.Article" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">

    <h2> <asp:Label id="lblArticle" runat="server" Text="Label"></asp:Label></h2>
   
    <asp:Image ID="imgPhoto" runat="server" AlternateText="" />

    <p>
    <asp:Label id="lblDescription" runat="server" Text="Label"></asp:Label>
    </p>

    <div>
        <p>
           Prix Unitaire :  <asp:Label id="lblPrix" runat="server" ></asp:Label> €
        </p>
        <p>
            Stock : <asp:Label id="lblStock" runat="server" ></asp:Label>
        </p>
    </div>
</asp:Content>
