﻿using AFCEPF.AI107.Boutique.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Boutique.DataAccess
{
    public class ArticleDAO : DAO
    {
        public List<ArticleEntity> GetAll()
        {
            List<ArticleEntity> result = new List<ArticleEntity>();

            // 2 - Préparer une commande SQL
            IDbCommand cmd = GetCommand(@"SELECT * 
                                            FROM article");

            try
            {
                // 3 - Ouvrir la connection
                cmd.Connection.Open();

                // 4 - Exécuter la commande
                IDataReader dr = cmd.ExecuteReader();

                // 5 - Traiter le résultat
                while (dr.Read())
                {
                    result.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // 6 - Fermer la connection
                cmd.Connection.Close();
            }
            return result;
        }

        public List<ArticleEntity> GetByIdRayon(int idRayon)
        {
            List<ArticleEntity> result = new List<ArticleEntity>();

            // 2 - Préparer une commande SQL
            IDbCommand cmd = GetCommand(@"SELECT * 
                                            FROM article 
                                            WHERE id_rayon = @idRayon");

            cmd.Parameters.Add(new MySqlParameter("@idRayon", idRayon));

            try
            {
                // 3 - Ouvrir la connection
                cmd.Connection.Open();

                // 4 - Exécuter la commande
                IDataReader dr = cmd.ExecuteReader();

                // 5 - Traiter le résultat
                while (dr.Read())
                {
                    result.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // 6 - Fermer la connection
                cmd.Connection.Close();
            }
            return result;
        }

        public void Insert(ArticleEntity nouveauArticle)
        {
            // 2 - Préparer une commande SQL
            IDbCommand cmd = GetCommand(@"INSERT INTO article (nom,description,prix_unitaire,stock,id_rayon) 
                                        VALUES (@nom, @description, @prixUnitaire, @stock, @idRayon)");

            cmd.Parameters.Add(new MySqlParameter("@nom", nouveauArticle.Nom));
            cmd.Parameters.Add(new MySqlParameter("@description", nouveauArticle.Description));
            cmd.Parameters.Add(new MySqlParameter("@prixUnitaire", nouveauArticle.PrixUnitaire));
            cmd.Parameters.Add(new MySqlParameter("@stock", nouveauArticle.Stock));
            cmd.Parameters.Add(new MySqlParameter("@idRayon", nouveauArticle.IdRayon));

            try
            {
                // 3 - Ouvrir la connection
                cmd.Connection.Open();

                // 4 - Exécuter la commande
                cmd.ExecuteNonQuery();
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // 6 - Fermer la connection
                cmd.Connection.Close();
            }
        }

        public ArticleEntity GetArticle(int id)
        {
            // L'entité doit être null au départ pour ne pas renvoyer un objet avec un id null
            ArticleEntity result = null;

            IDbCommand cmd = GetCommand(@"SELECT * 
                                            FROM article 
                                            WHERE id = @id");

            cmd.Parameters.Add(new MySqlParameter("@id", id));

            try
            {
                // 3 - Ouvrir la connection
                cmd.Connection.Open();

                // 4 - Exécuter la commande
                IDataReader dr = cmd.ExecuteReader();

                // 5 - Traiter le résultat
                if (dr.Read()) // Si quelques chose à lire
                {
                    result = DataReaderToEntity(dr);
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // 6 - Fermer la connection
                cmd.Connection.Close();
            }
            return result;

        }


        private ArticleEntity DataReaderToEntity(IDataReader dr)
        {
            ArticleEntity article = new ArticleEntity();
            article.Id = dr.GetInt32(dr.GetOrdinal("id"));
            article.Nom = dr.GetString(dr.GetOrdinal("nom"));
            if (!dr.IsDBNull(dr.GetOrdinal("description")))
            {
                article.Description = dr.GetString(dr.GetOrdinal("description"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("prix_unitaire")))
            {
                article.PrixUnitaire = dr.GetFloat(dr.GetOrdinal("prix_unitaire"));
            }
            article.Stock = dr.GetInt32(dr.GetOrdinal("stock"));
            if (!dr.IsDBNull(dr.GetOrdinal("photo")))
            {
                article.Photo = dr.GetString(dr.GetOrdinal("photo"));
            }
            // Si un champ peut être null en base, il faut le vérifier 
            // AVANT de le lire 
            if (!dr.IsDBNull(dr.GetOrdinal("id_rayon")))
            {
                article.IdRayon = dr.GetInt32(dr.GetOrdinal("id_rayon"));
            }
            return article;
        }

    }
}
