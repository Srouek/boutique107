﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Boutique.DataAccess
{
    public abstract class DAO
    {
        private const string connectionString = "Server=localhost;Database=boutique;Uid=root;Pwd=root;";

        protected IDbCommand GetCommand(string sql)
        {
            // 1 - Configurer une connection
            IDbConnection cnx = new MySqlConnection();
            cnx.ConnectionString = connectionString;
            
            IDbCommand cmd = new MySqlCommand();
            cmd.Connection = cnx;
            cmd.CommandText = sql;

            return cmd;
        }
    }
}
