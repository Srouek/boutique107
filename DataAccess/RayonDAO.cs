﻿using AFCEPF.AI107.Boutique.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;

namespace AFCEPF.AI107.Boutique.DataAccess
{
    public class RayonDAO : DAO
    {
        
        public List<RayonEntity> GetAll()
        {
            List<RayonEntity> result = new List<RayonEntity>();

            // 2 - Préparer une commande SQL
            IDbCommand cmd = GetCommand("SELECT * FROM rayon");

            try
            {
                // 3 - Ouvrir la connection
                cmd.Connection.Open();

                // 4 - Exécuter la commande
                IDataReader dr = cmd.ExecuteReader();

                // 5 - Traiter le résultat
                while (dr.Read())
                {
                    RayonEntity rayon = new RayonEntity();
                    rayon.Id = dr.GetInt32(dr.GetOrdinal("id"));
                    rayon.Libelle = dr.GetString(dr.GetOrdinal("libelle"));
                    result.Add(rayon);
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // 6 - Fermer la connection
                cmd.Connection.Close();
            }
            return result;
        }

        public void Insert(RayonEntity nouveauRayon)
        {
            // 2 - Préparer une commande SQL
            IDbCommand cmd = GetCommand(@"INSERT INTO rayon (libelle) 
                                        VALUES (@libelle)");

            cmd.Parameters.Add(new MySqlParameter("@libelle", nouveauRayon.Libelle));

            try
            {
                // 3 - Ouvrir la connection
                cmd.Connection.Open();

                // 4 - Exécuter la commande
                cmd.ExecuteNonQuery();
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // 6 - Fermer la connection
                cmd.Connection.Close();
            }


        }
    }
}
