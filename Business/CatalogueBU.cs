﻿
using AFCEPF.AI107.Boutique.DataAccess;
using AFCEPF.AI107.Boutique.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Boutique.Business
{
    public class CatalogueBU
    {
        #region RAYONS
        public List<RayonEntity> GetListeRayons()
        {
            RayonDAO dao = new RayonDAO();
            return dao.GetAll();
        }

        public void AjouterRayon(RayonEntity nouveauRayon)
        {
            bool trouve = false;
            RayonDAO dao = new RayonDAO();
            // vérifier que le libellé est unique :
            foreach (RayonEntity r in dao.GetAll())
            {
                if (r.Libelle == nouveauRayon.Libelle)
                {
                    trouve = true;
                }
            }

            if (!trouve)
            {
                // inserer un rayon
                dao.Insert(nouveauRayon);
            }
            else
            {
                throw new Exception("ERREUR : Rayon déjà existant");
            }
        }

        public void AjouterArticle(ArticleEntity nouveauArticle)
        {
            bool trouve = false;
            ArticleDAO dao = new ArticleDAO();
            // vérifier que le libellé est unique :
            foreach (ArticleEntity a in dao.GetAll())
            {
                if (a.Nom == nouveauArticle.Nom)
                {
                    trouve = true;
                }
            }

            if (!trouve)
            {
                // inserer un rayon
                dao.Insert(nouveauArticle);
            }
            else
            {
                throw new Exception("ERREUR : Article déjà existant");
            }
        }

        public ArticleEntity GetArticle(bool v1, out int v2)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region ARTICLES

        public List<ArticleEntity> GetListeArticles()
        {
            return GetListeArticles(-1);
        }

        public List<ArticleEntity> GetListeArticles(int idRayon)
        {
            ArticleDAO dao = new ArticleDAO();
            if (idRayon == -1)
            {
                return dao.GetAll();
            }
            else
            {
                return dao.GetByIdRayon(idRayon);
            }
        }

        public ArticleEntity GetArticle(int id)
        {
            ArticleDAO dao = new ArticleDAO();
            return dao.GetArticle(id);
        }

        #endregion
    }
}
